# Android #


* io.reactivex.rxjava2:rxkotlin - https://github.com/ReactiveX/RxKotlin
* org.ethereum:ethereumj-core - https://github.com/ethereum/ethereumj
* com.squareup.retrofit2 - https://github.com/square/retrofit
* com.squareup.okhttp3 - https://github.com/square/okhttp
* com.github.Tolriq:retrofit-jsonrpc - https://github.com/Tolriq/retrofit-jsonrpc
* com.github.VictorAlbertos.RxCache - https://github.com/VictorAlbertos/RxCache
* com.github.VictorAlbertos.Jolyglot - https://github.com/VictorAlbertos/Jolyglot

* com.dlazaro66.qrcodereaderview:qrcodereaderview - https://github.com/dlazaro66/QRCodeReaderView
* io.reactivex.rxjava2:rxandroid - https://github.com/ReactiveX/RxAndroid
* com.bottlerocketstudios:vault - https://github.com/BottleRocketStudios/Android-Vault
* com.hbb20:ccp - https://github.com/hbb20/CountryCodePickerProject
* com.jakewharton.rxbinding2:rxbinding - https://github.com/JakeWharton/RxBinding
* com.jakewharton:butterknife - https://github.com/JakeWharton/butterknife
* com.squareup.picasso:picasso - https://github.com/square/picasso
* com.ramotion.foldingcell:folding-cell - https://github.com/Ramotion/folding-cell-android
* com.github.medyo:fancybuttons - https://github.com/medyo/Fancybuttons
* com.wang.avi:library - https://github.com/81813780/AVLoadingIndicatorView
* com.github.ittianyu:BottomNavigationViewEx - https://github.com/ittianyu/BottomNavigationViewEx
* net.yslibrary.keyboardvisibilityevent:keyboardvisibilityevent - https://github.com/yshrsmz/KeyboardVisibilityEvent
* * com.andkulikov:transitionseverywhere - https://github.com/andkulikov/Transitions-Everywhere
* com.github.christophesmet:android_maskable_layout - https://github.com/christophesmet/android_maskable_layout
